var clientesObtenidos;

function getClientes() {
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    if (this.readyState == 4 && this.status == 200) {
      //console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarClientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("tablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-stripped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");

    var columnaContacto = document.createElement("td");
    columnaContacto.innerText = JSONClientes.value[i].ContactName;

    var columnaCity = document.createElement("td");
    columnaCity.innerText = JSONClientes.value[i].City;

    var columnaCountry = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
     if (JSONClientes.value[i].Country == "UK"){
       imgBandera.src = imgBandera.src + "United-Kingdom.png";
     }else{
       imgBandera.src = imgBandera.src + JSONClientes.value[i].Country + ".png";
     }
     imgBandera.classList.add("flag");

    columnaCountry.appendChild (imgBandera);

    nuevaFila.appendChild(columnaContacto);
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(columnaCountry);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
